function validateForm() {
    const nameError = document.getElementById('name-error').innerText;
    if (nameError === 'true') {
        document.getElementById('name-error').innerText = '';
        document.getElementById('signup-response').innerText = `Please check entered details before submitting`;
        return false;
    }
    else {
        document.getElementById('signup-response').innerText = ``;
        localStorage.setItem('name',document.getElementById('first-name').value);
        window.location.href='./index.html';
        return true;
    }
}

document.getElementById('first-name').addEventListener('change', () => {
    checkValid(document.getElementById('first-name'), 'fname-error');
});


document.getElementById('last-name').addEventListener('change', () => {
    checkValid(document.getElementById('last-name'), 'lname-error');
});

function checkValid(name, id) {
    if (name.value.length > 30 || name.value.length < 2) {
        document.getElementById('name-error').innerText = `true`;
        name.style.background = '#fab2ac';
        document.getElementById(id).innerText = `Name length must be between 2 to 30 characters`;
    }
    else if (name.value.match(/[0-9]+/) || name.value.match(/[$@#&!]+/)) {
        document.getElementById('name-error').innerText = `true`;
        name.style.background = '#fab2ac';
        document.getElementById(id).innerText = `Name cannot contain number or special characters`;
    }
    else {
        name.style.background = '#fff';
        document.getElementById(id).innerText = ``;
    }
}

document.getElementById('email').addEventListener('change', () => {
    const emailRegex = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
    const isValidEmail = emailRegex.test(document.getElementById('email').value);

    if (!isValidEmail) {
        document.getElementById('name-error').innerText = `true`;
        document.getElementById('email-error').innerText = `Please enter correct email address`;
        document.getElementById('email').style.background = '#fab2ac';
    }
    else {
        document.getElementById('email').style.background = '#fff';
        document.getElementById('email-error').innerText = ``;
        document.getElementById('signup-response').innerText = ``;
    }
});
document.getElementById('password1').addEventListener('change', () => {
    const password1 = document.getElementById('password1').value;
    if (password1.length < 8 || !(password1.match(/[a-z]+/)) || !(password1.match(/[A-Z]+/)) || !(password1.match(/[0-9]+/)) || !(password1.match(/[$@#&!]+/))) {
        document.getElementById('password1-error').innerText = `Password is not strong enough`;
        document.getElementById('name-error').innerText = `true`;
        document.getElementById('password1').style.background = '#fab2ac';
    }
    else {
        document.getElementById('password1').style.background = '#fff';
        document.getElementById('password1-error').innerText = ``;
    }
})
document.getElementById('password2').addEventListener('change', () => {
    const password1 = document.getElementById('password1').value;
    const password2 = document.getElementById('password2').value;
    if (password1 !== password2) {
        document.getElementById('password2-error').innerText = `Password did not match: Please try again...`;
        document.getElementById('name-error').innerText = `true`;
        document.getElementById('password2').style.background = '#fab2ac';
    }
    else {
        document.getElementById('password2').style.background = '#fff';
        document.getElementById('password2-error').innerText = ``;
    }
})