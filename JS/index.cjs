if (localStorage.getItem('name') !== null) {

    document.getElementById('user').innerText = ` 👤 ${localStorage.getItem('name')}`;
    document.getElementById('user-li').style.display = 'block';
    document.getElementById('login').innerText='Logout';
    login.addEventListener('click', () => {
        localStorage.clear();
        location.reload();
    });
}
else{
    document.getElementById('user').innerText = '';
    document.getElementById('user-li').style.display = 'none';
}


fetch('https://fakestoreapi.com/products')
    .then((res) => {
        return res.json();
    })
    .then((data) => {
        document.getElementById('container').innerText = "";
        let productsDiv = data.map((product) => {
            const products = document.createElement('div');
            products.className = 'product';

            const img = document.createElement(`img`);
            img.src = `${product.image}`;

            const productsName = document.createElement('h4');
            productsName.className = 'product-name';
            productsName.innerText = `${product.title}`;

            const price = document.createElement('p');
            price.className = 'price';
            price.innerText = `💲 ${product.price}`;

            const category = document.createElement('p');
            category.className = "category"
            category.innerText = `🏷️ ${product.category}`;

            const { rate, count } = product.rating;

            const productRate = document.createElement('p');
            productRate.innerText = `Rating : ${rate} ⭐ (${count})`;

            const addToCart = document.createElement('p');
            addToCart.innerText = `Add to	🛒`
            addToCart.className = "addToCart";

            products.appendChild(img);
            products.appendChild(category);
            products.appendChild(productsName);
            products.appendChild(price);
            products.appendChild(productRate);
            products.appendChild(addToCart);
            return products;
        });
        document.getElementById('container').append(...productsDiv);
    })
    .catch((err) => {
        document.getElementById('container').innerText = "";
        const error = document.createElement('div');
        error.className = 'error';

        const serverdied = document.createElement('div');
        serverdied.className = 'error-graphics'

        const message = document.createElement('h1');
        message.className = 'message';
        message.innerText = "We are facing Some Internal server error, Please visit after some time"

        error.appendChild(message);
        error.appendChild(serverdied);

        document.getElementById('container').appendChild(error);
    });